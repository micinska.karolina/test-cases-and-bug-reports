## Bug report 1 

**Bug Title:** Main Menu > Flavours > Netflix original flavours > I stream you stream - Recent Tweets box is not loading  

**Bug Category:** Content issue  

**Severity:** Low  

**Occurrence:** Always - can be reproduced at all times  

**Testing device:** Windows 10, Chrome  

Created on: Wednesday, February 5, 2020 at 11:52 AM  
Submitted at: Wednesday, February 5, 2020 at 11:52 AM

**Steps that led to the Bug:**  
1. Open https://www.benjerry.co.uk
2. Mouse over "Flavours" in main manu
3. Click on "Netflix Original Flavours"
4. Click "I Stream, You Stream: The Wildest Questions We’ve Ever Been Asked" on articles carousel
5. Scroll page to "Recent Tweets" box  

**Expected Result:**  
Box "Recent Tweets" is connected with Twitter page and there are some Ben & Jerry's twitter entries.  

**Actual Result:**  
There are no recent or any tweets listed, the content is not loading properly (box is blank).  
**Screenshots:**
![alt text](screenshots/1a.png)  
![alt text](screenshots/1.png)  