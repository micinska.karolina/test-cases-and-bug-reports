## Report Overview 

Created on: Wednesday, February 5, 2020 at 10:19:44 AM  
Testing device: Windows 10, Chrome  

User Case 1: UC You want to learn about Ben and Jerry's "Flavour Gurus" under the flavours tab. Show your results by uploading one screenshot – show your whole screen including URL and taskbar. You MUST be using https://www.benjerry.co.uk/  

Screenshot 1:  
![alt text](screenshots/a.png)  

User Case 2: You want to find the ingredients for a specific Ben and Jerry's product. Please use the search bar or the filter tool on the Flavours page. Find the ingredients information about a specific ice cream product and list your steps here.
1. Open https://www.benjerry.co.uk/
2. Mouse over "Flavours" in navigation menu
3. Click on "Our flavours"
4. Click on "Ice Cream Tubs" product image
5. Click on "Baked Alaska" product image
6. Scroll to "Ingredients"
7. Click on plus symbol  

Screenshot 2:  
![alt text](screenshots/aa.png)  

UC 3: You want to learn more about the company’s history. Try to find the year the first Ben and Jerry’s ice cream shop was opened. How did you find this information? List your steps.
1. Open https://www.benjerry.co.uk/
2. Click on "About us" in navigation menu
3. Scroll down the page

Screenshot 3:  
![alt text](screenshots/aaa.png)  

UC 4: You want to get driving directions to a specific Ben and Jerry's Scoop Shop (ice cream shop) in Manchester, United Kingdom. There's more than one way to find this information. Please explore the test site now and explain the steps you took to find a Scoop Shop and driving directions (be sure to list every step).
1. Open https://www.benjerry.co.uk/
2. Mouse over "Scoop Shops"
3. Click on "Find an Ice Cream Shop"
4. Click on "Scoop Shops"
5. Page refused to load the script

Screenshot 4:  
![alt text](screenshots/b.png)

UC 5: Explore another section of the website that interests you and you haven't been to in any of the other Use Cases (e.g. Values, Movies, What’s New, etc.). What section did you explore? List at least 5 different actions (e.g. scrolled/tapped/clicked).
1. Open https://www.benjerry.co.uk/
2. Click on "What's New" in navigation menu
3. Scroll down the page
4. Tap on "load more"
5. Click on "Read more" on article "Top 10 Ben & Jerry’s Flavors Of 2019"  

Screenshot 5:  
![alt text](screenshots/bb.png)  

Q1: Which features/elements of the website did you like? And why (with details and examples)? Minimum of 3 different points.
1. Every title and buttons content on page have clear meaning and lead to expected content.
2. It's easy to navigate on page, I can easly find content, e.g. I want to know about specyfic kind of ice creams so I just need two clicks from "Flavours" menu to satifsfying content.  
3. Colours of backgrounds and fonts are calm and readable; fonts are all black and backgrounds are in bright colours so the reading process is easy.  

Q2: Which features/elements of the website did you dislike? And why (with details and examples)? Minimum of 3 different points.  
1. Drop down navigation menu doesn't hide without clicking and stays unrolled, covering content. 
2. Formatting and editing text in articles aren't unified, e.g. compare "Fairtrade" article with "I Stream, You Stream: The Wildest Questions We’ve Ever Been Asked" article with "8 Tips to Keep Unwanted Hands Off Your Topped Tub" > every single one has different style of editing, some text moves, another don't.  
3. Image style and editing in articles > often incorrect text flow around image, there is too much free space around the image, there are different types of "decorating" of images (some have white frame around, another don't).  

Q3: If you were responsible for the website, what would you change about it and why? (Give details and/or examples)  
For me it would be better if drop down navigation menu list hides itself when there is no mouse over it and the full content is visible without clicking (at the moment it stays unrolled).  

Another thing is that I can assume that articles are being written by different people with different formatting and editing styles. I would definitely go with unifying formating text, images, bullet lists, fonts.   