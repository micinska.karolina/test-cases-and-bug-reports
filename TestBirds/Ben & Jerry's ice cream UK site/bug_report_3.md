## Bug report 3

**Bug Title:** Cone Together article page > missing image (status 404)  

**Bug Category:** Content issue  

**Severity:** Low  

**Occurrence:** Always - can be reproduced at all times  

**Testing device:** Windows 10, Chrome  

Created on	Tuesday, February 4, 2020 at 8:39 PM  
Submitted at	Tuesday, February 4, 2020 at 8:41 PM  

**Steps that led to the Bug:**  
1. Open https://www.benjerry.co.uk/
2. Mouse over "Values" in main menu
3. Click on "Issues we care about"
4. Click on "Refugees"
5. Click on "Articles"
6. Click on left arrow three times
7. Click on "Cone together" article  

**Expected Result:**  
After the page with article is fully loaded the main image of the article should show up  

**Actual Result:**  
The image under the title of article is showing broken icon  


**Screenshots:**  
![alt text](screenshots/3.png)