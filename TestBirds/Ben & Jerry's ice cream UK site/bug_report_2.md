## Bug report 2

**Bug Title:** Community Involvement page > Incorrect text flow around image  

**Bug Category:** Display Error  

**Severity:** Low  

**Occurrence:** Always - can be reproduced at all times  

**Testing device:** Windows 10, Chrome  

Created on: Wednesday, February 5, 2020 at 10:46 AM  
Submitted at: Wednesday, February 5, 2020 at 10:46 AM  

**Steps that led to the Bug:**  
1. Open https://www.benjerry.co.uk
2. Mouse over "Values"
3. Click "How we do business"
4. Click on left arrow on carousel to the "People, Community & Giving Back Practises"
5. Click on "Community Action"  

**Expected Result:**  
Image alignment with text  

**Actual Result:**
Incorrect text flow around image > image is placed too high, there is too much free space under the image  


**Screenshots:**  
![alt text](screenshots/2.png)

