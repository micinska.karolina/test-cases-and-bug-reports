## PT9: logowanie użytkownika za pomocą niepoprawnych danych  

Opis: niezalogowany użytkownik chce się zalogować za pomocą nieprawidłowego adresu email  

Środowisko: Windows 10 Home, wer. 10.018362, przeglądarka Google Chrome wer. 78.0.3904.70  

Warunki początkowe: użytkownik jest niezalogowany  

Priorytet: niski 🔸  
Ważność: niska 🔸  

Kroki:  
1. wejdź na stronę główną http://automationpractice.com/index.php
2. kliknij "Sing in"
3. wpisz w polu "Email address" niepoprawny adres email (np. stops@gmail.com)
4. wpisz w polu "Password" poprawne hasło (patrz. PT1)
5. kliknij "Sing in"  

Oczekiwany rezultat: użytkownik pozostanie niezalogowany, wyświetla się komunikat o wpisaniu niepoprawnego adresu email  

Aktualny rezultat: użytkownik pozostanie niezalogowany, wyświetla się komunikat o błędzie autoryzacji (nie wiadomo, co zostało niepoprawnie wpisane)  

Screenshot: 
![alt text](screenshots/pt9.png)