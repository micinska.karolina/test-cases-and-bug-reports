## zgloszenie1: odnośnik reklamy nie przenosi na na podstronę z promocją  

Opis: Kliknięcie na reklamę prowadzi do przeniesienia użytkownika do odpowiedniego miejsca w serwisie  

Środowisko: Windows 10 Home, wer. 10.018362, przeglądarka Google Chrome wer. 78.0.3904.70  

Warunki początkowe: użytkownik jest niezalogowany  

Priorytet: niski 🔸  
Ważność: wysoka 🔸🔸🔸  

Kroki:  
1. wejdź na stronę http://automationpractice.com/index.php
2. kliknij na reklamę "SALE get up to -25% OFF"  

Oczekiwany rezultat: użytkownik jest przekierowany do podstrony z reklamowaną promocją  

Aktualny rezulat: użytkownik jest przekierowany do strony poza serwisem (https://www.prestashop.com/pl)  

Screenshot 1:  
![alt text](screenshots/pt7a.png)  
Screenshot 2:  
![alt text](screenshots/pt7b.png)