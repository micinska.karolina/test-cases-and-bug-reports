## PT10: sprawdzenie pojawienia się komunikatu o braku wyszukiwanego produktu  

Opis: użytkownik wpisuje w pole wyszukiwania produkt, którego nie ma w sklepie  

Środowisko: Windows 10 Home, wer. 10.018362, przeglądarka Google Chrome wer. 78.0.3904.70 64-bitowa, antywirus UPC by F-Secure  

Ważność: niska 🔸  
Priorytet: niski 🔸  

Warunki początkowe: użytkownik niezalogowany  
Dane wejściowe: pole wyszukiwania = "blabla"

Kroki:  
1. wejdź na stronę http://automationpractice.com/index.php 
2. wpisz w polu wyszukiwania dane wejściowe
3. wciśnij na klawiaturze enter  

Oczekiwany rezultat: strona wyświetli komunikat o braku wyszukiwanego produktu  

Screenshot:  
![alt text](screenshots/pt10.png)