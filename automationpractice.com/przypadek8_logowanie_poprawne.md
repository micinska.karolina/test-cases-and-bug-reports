## PT8: Logowanie użytkownika za pomocą poprawnych danych  

Opis: niezalogowany użytkownik chce się zalogować wpisując poprawne dane  

Środowisko: Windows 10 Home, wer. 10.018362, przeglądarka Google Chrome wer. 78.0.3904.70  

Warunki początkowe: użytkownik jest niezalogowany  

Priorytet: niski 🔸  
Ważność: wysoka 🔸🔸🔸  

Kroki:  
1. wejdź na stronę http://automationpractice.com/index.php
2. kliknij przycisk "Sing in"
3. wpisz w polu "Email address" adres podany przy rejestracji (patrz. PT1)
4. wpisz w polu "Password" hasło podane przy rejestracji (patrz. PT1)
5. kliknij przycisk "Sing in"  

Oczekiwany rezultat: użytkownik jest zalogowany
 