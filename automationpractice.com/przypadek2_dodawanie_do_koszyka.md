## TC2: dodawanie produktu do koszyka

Opis: użytkownik ma możliwość dodania produktu do koszyka  

Środowisko: Windows 10 Home, wer. 10.018362, przeglądarka Google Chrome wer. 78.0.3904.70 64-bitowa, antywirus UPC by F-Secure  

Warunki początkowe: użytkownik niezalogowany  

Ważność: średnia 🔸🔸  
Priorytet: średni 🔸🔸  

Kroki:  
1. wejdź na stronę http://automationpractice.com/index.php 
2. najedź kursorem w menu na zakładkę "women"
3. kliknij w zakładkę "blouses"
4. najedź kursorem na dostępny produkt (czarna bluzka)
5. kliknij niebieski przycisk "add to cart"  

Oczekiwany rezultat: kliknięcie spowoduje dodanie produktu do koszyka i wyświetlenie komunikatu „Product successfully added to your shopping cart”