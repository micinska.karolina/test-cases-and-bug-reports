## PT4: znaki wpisywane w polu wyszukiwania znikają za ikoną "lupy"  

Środowisko: Windows 10 Home, wer. 10.018362, przeglądarka Google Chrome wer. 78.0.3904.70 64-bitowa, antywirus UPC by F-Secure  

Warunki początkowe: użytkownik niezalogowany  

Priorytet: niski 🔸  
Ważność: niski 🔸  

Kroki:  
1. wejdź na stronę http://automationpractice.com/index.php 
2. wpisz ciąg znaków w pole wyszukiwania poza obszar pola  

Oczekiwany rezultat: wpisany tekst przesunie się w polu wraz z postępem wpisywania  

Aktualny rezultat: tekst "chowa się" za ikoną wyszukiwania i nie jest wiadomo, co jest wpisywane  

Screenshot 1:  
![alt text](screenshots/pt4.png)