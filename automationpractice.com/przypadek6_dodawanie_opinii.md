## TC4: dodawanie opinii o produkcie  

Opis: użytkownik ma możliwość dodania opinii o produkcie na stronie produktu  

Środowisko: Windows 10 Home, wer. 10.018362, przeglądarka Google Chrome wer. 78.0.3904.70  

Warunki początkowe: użytkownik jest zalogowany  

Priorytet: niski 🔸  
Ważność: niski 🔸  

Kroki:  
6. wejdź na stronę http://automationpractice.com/index.php 
7. najedź kursorem w menu na zakładkę "women"
8. kliknij w zakładkę "blouses"
9. kliknij na zdjęcie dostępnego produktu (czarna bluzka)
10. jesteś na stronie produktu
11. kliknij szary przycisk na dole strony "Be the first to write your review!"
12. wyświetli się okno "write a review"
13. wpisz w pole tekstowe "title": czarna bluzka
14. wpisz w pole tekstowe "comment": ładna 
15. kliknij szary przycisk "send"  

Oczekiwany rezultat: wyświetli się komunikat o dodaniu opinii po zaakceptowaniu przez moderatora "Your comment has been added and will be available once approved by a moderator"