## TC3: wyszukiwanie produktu w wyszukiwarce  

Opis: użytkownik ma możliwość wyszukiwania produktów w pasku wyszukiwania  

Środowisko: Windows 10 Home, wer. 10.018362, przeglądarka Google Chrome wer. 78.0.3904.70 64-bitowa, antywirus UPC by F-Secure  

Ważność: średnia 🔸🔸  
Priorytet: niski 🔸  

Warunki początkowe: użytkownik niezalogowany  
Dane wejściowe: pole wyszukiwania = "dress"  

Kroki:  
1. wejdź na stronę http://automationpractice.com/index.php 
2. wpisz w polu wyszukiwawania dane wejściowe
3. wciśnij na klawiaturze enter  

Oczekiwany rezultat: strona przekieruje do wyników wyszukiwania produktów w kategorii sukienki  

Aktualny rezultat: w wynikach wyszukiwania znalazł się też produkt z innej kategorii wyszukiwania (bluzka)  

Screenshot 1:  
![alt text](screenshots/pt3a.png)  

Screenshot 2:  
![alt text](screenshots/pt3b.png)  