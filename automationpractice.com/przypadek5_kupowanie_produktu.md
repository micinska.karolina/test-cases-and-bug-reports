## TC5: kupowanie produktu  

Opis: użytkownik ma możliwość dodania produktu do koszyka oraz finalizacji zakupu  

Środowisko: Windows 10 Home, wer. 10.018362, przeglądarka Google Chrome wer. 78.0.3904.70 64-bitowa, antywirus UPC by F-Secure  

Warunki początkowe: użytkownik jest zalogowany  

Ważność: wysoka 🔸🔸🔸  
Priorytet: wysoki 🔸🔸🔸  

Kroki:  
1. wejdź na stronę http://automationpractice.com/index.php 
2. najedź kursorem w menu na zakładkę "women"
3. kliknij w zakładkę "blouses"
4. najedź kursorem na dostępny produkt (czarna bluzka)
5. kliknij niebieski przycisk "add to cart"
6. kliknij w prawym górnym rogu strony przycisk "cart" z symbolem wózka sklepowego
7. jesteś na stronie koszyka z produktem
8. kliknij zielony przycisk na dole strony "proceed to checkout"
9. jesteś na stronie z adresem podanym przy rejestracji
10. kliknij zielony przycisk na dole strony "proceed to checkout"
11. jesteś na stronie ze sposobami dostawy
12. zaznacz checkbox "I agree to the terms of service and will adhere to them unconditionally."
13. kliknij zielony przycisk na dole strony "proceed to checkout"
14. jesteś na stronie ze sposobami płatności
15. kliknij przycisk z płatnością "pay by bank wire"
16. jesteś na stronie z potwierdzeniem
17. kliknij "I confirm my order"  

Oczekiwany rezultat: kliknięcie spowoduje sfinalizowanie transakcji i wyświetlenie strony z danymi do przelewu  