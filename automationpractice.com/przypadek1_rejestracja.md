## PT1: tworzenie konta

Opis: użytkownik chce się zarejestrować   

Środowisko: Windows 10 Home, wer. 10.018362, przeglądarka Google Chrome wer. 78.0.3904.70 64-bitowa, antywirus UPC by F-Secure  

Ważność: wysoka 🔸🔸🔸  
Priorytet: średni 🔸🔸  

Warunki początkowe: użytkownik niezarejestrowany  

Kroki:  
1. wejdź na stronę http://automationpractice.com/index.php 
2. kliknij przycisk "sign in" w prawym górnym rogu ekranu 
3. wpisz adres email - stop02@gmail.com - w polu testowym po lewej stronie
4. kliknij pomarańczowy przycisk "create an account"
5. zaznacz radio button "Mrs."
6. wpisz w polu "first name" wartość: Janina
7. wpisz w polu "last name" wartość: Nowak
8. wpisz w polu "password" wartość: jn00000
9. wybierz z listy rozwijalnej "date of birth" dzień: 1
10. wybierz z listy rozwijalnej "date of birth" miesiąc: January
11. wybierz z listy rozwijalnej "date of birth" rok: 2001
12. wpisz w polu tekstowym "adresses": Kwiatowa
13. wpisz w polu tesktowym "adresses line 2": 100a
14. wpisz w polu tekstowym "city": Katowice
15. wybierz z listy rozwijalnej "state": Alabama
16. wpisz w polu tekstowym "zip/postal code": 12345
17. wpisz w polu tekstowym "home phone": 123456789
18. kliknij zielony przycisk "register"  

Oczekiwany rezultat: użytkownik zostanie zarejestrowany i automatycznie zalogowany, a w prawym górnym rogu pojawi się jego imię i nazwisko (Janina Nowak)