## PT1: Użytkownik portalu zus.pl może się zalogować  

Opis: Użytkownik posiadający profil zaufania może zalogować się do portalu zus.pl  

Środowisko: Windows 10 Home, wer. 10.018362, przeglądarka Google Chrome wer. 78.0.3904.70  

Priorytet: średni 🔸🔸  
Ważność: wysoka 🔸🔸🔸  

Warunki wstępne: Użytkownik nie jest zalogowany  

Kroki:
1. Wejdź na stronę https://www.zus.pl
2. Kliknij "Zaloguj się przez profil zaufany/login.gov.pl"
3. Kliknij "Profil zaufany"
4. Wpisz poprawną nazwę użytkownika w polu "Nazwa użytkownika lub e-mail"
5. Wpisz poprawne hasło w polu "Hasło"
6. Kliknij "Zaloguj"  

Oczekiwany rezultat: Użytkownik loguje się do serwisu zus.pl profilem zaufanym  

Warunki wyjściowe: Użytkownik jest zalogowany, znajdujemy się na stronie portalu