PT4: Użytkownik może zalogować się do zus.pl używając loginu i hasła  

Opis: Jako użytkownik posiadający konto w portalu zus.pl mogę zalogować się do portalu używając poprawnego loginu i hasła  

Środowisko: Chrome, Windows 10  

Warunki wstępne: Użytkownik nie jest zalogowany i znajduje się na stronie https://www.zus.pl/portal/logowanie.npi  

Kroki:  
1. Wpisz poprawny Login
2. Wpisz poprawne Hasło
3. Kliknij w przycisk "Zaloguj"  

Oczekiwany rezultat: Użytkownik zostaje poprawnie zalogowany do serwisu i przekierowany do portalu  

Warunki wyjściowe: Użytkownik jest zalogowany i znajduje się w portalu zus.pl  