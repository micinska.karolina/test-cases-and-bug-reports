## PT3: Użytkownik nie może się zalogować do zus.pl z powodu źle podanego loginu  

Opis: Użytkownik posiadający konto w portalu zus.pl nie może się zalogować  

Warunki wstępne: Użytkownik nie jest zalogowany, znajduje się na stronie https://www.zus.pl/portal/logowanie.npi  

Kroki:  
1. Wpisz niepoprawny login
2. Wpisz poprawne hasło 
3. Kliknij przycisk "zaloguj"  

Oczekiwany rezultat: Użytkownik nie zaloguje się do serwisu, pojawia się komunikat "Nieprawidłowy login lub hasło."  

Warunki wyjściowe: Użytkownik nie jest zalogowany  

Screenshot:  
![alt text](screenshots/przy2.png)