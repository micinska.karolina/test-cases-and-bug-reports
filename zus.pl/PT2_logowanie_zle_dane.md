## PT2: Użytkownik posiadający konto w portalu zus.pl nie może się zalogować z powodu nieuzupełnienia pola loginu 

Opis: Użytkownik posiadający konto w portalu zus.pl nie może się zalogować  

Środowisko: Chrome, Windows 10

Warunki wstępne: Użytkownik nie jest zalogowany, znajduje się na stronie https://www.zus.pl/portal/logowanie.npi  


Kroki:  
1. Pole login pozostaje puste
2. Wpisz poprawne hasło 
3. Kliknij przycisk "zaloguj"  

Oczekiwany rezultat: Użytkownik nie zaloguje się do serwisu, pojawia się komunikat "Ta wartość jest wymagana."  

Warunki wyjściowe: Użytkownik nie jest zalogowany, jest ponownie na stronie logowania  

Screenshot:
![alt text](screenshots/przy1.png)